package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/arn"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ecs"
	"github.com/jedib0t/go-pretty/table"
)

type Task struct {
	arn     *string
	cluster string
}

func (t *Task) Stop(svc *ecs.ECS) error {
	stopTask := &ecs.StopTaskInput{
		Cluster: aws.String(t.cluster),
		Task:    t.arn,
	}
	_, err := svc.StopTask(stopTask)
	return err
}

func (t *Task) Data(svc *ecs.ECS) *ecs.Task {
	describeTask := &ecs.DescribeTasksInput{
		Cluster: aws.String(t.cluster),
		Tasks:   []*string{t.arn},
	}
	data, err := svc.DescribeTasks(describeTask)
	if err != nil {
		handleAwsErr(err)
		panic(err)
	}
	if len(data.Tasks) != 1 {
		log.Panicf("Expected only one task. Got: %d", len(data.Tasks))
	}
	return data.Tasks[0]
}

func (t *Task) statusFromData(svc *ecs.ECS, data *ecs.Task) string {
	status := *data.LastStatus

	if status == AWS_STOPPED {
		status += ": " + *data.StoppedReason
	}
	return status

}

func (t *Task) Status(svc *ecs.ECS) string {
	return t.statusFromData(svc, t.Data(svc))

}
func handleAwsErr(err error) {
	if aerr, ok := err.(awserr.Error); ok {
		switch aerr.Code() {
		case ecs.ErrCodeServerException:
			fmt.Println(ecs.ErrCodeServerException, aerr.Error())
		case ecs.ErrCodeClientException:
			fmt.Println(ecs.ErrCodeClientException, aerr.Error())
		case ecs.ErrCodeInvalidParameterException:
			fmt.Println(ecs.ErrCodeInvalidParameterException, aerr.Error())
		default:
			fmt.Println(aerr.Error())
		}
	} else {
		// Print the error, cast err to awserr.Error to get the Code and
		// Message from an error.
		fmt.Println(err.Error())
	}

}

const AWS_RUNNING = "RUNNING"
const AWS_STOPPED = "STOPPED"
const AWS_PENDING = "PENDING"

func listClusterTasks(svc *ecs.ECS, clusterName string) []*Task {
	var taskList []*Task
	for _, status := range []string{AWS_RUNNING, AWS_STOPPED, AWS_PENDING} {
		params := &ecs.ListTasksInput{
			Cluster:       aws.String(clusterName),
			DesiredStatus: &status,
		}
		tasks, err := svc.ListTasks(params)
		if err != nil {
			handleAwsErr(err)
			return nil
		}
		for _, task := range tasks.TaskArns {
			taskList = append(taskList, &Task{
				arn:     task,
				cluster: clusterName,
			})
		}
	}
	return taskList

}

const IndexName = "docker.io"

// splitReposSearchTerm breaks a search term into an index name and remote name
func splitReposSearchTerm(reposName string) (string, string) {
	nameParts := strings.SplitN(reposName, "/", 2)
	var indexName, remoteName string
	if len(nameParts) == 1 || (!strings.Contains(nameParts[0], ".") &&
		!strings.Contains(nameParts[0], ":") && nameParts[0] != "localhost") {
		// This is a Docker Index repos (ex: samalba/hipache or ubuntu)
		// 'docker.io'
		indexName = IndexName
		remoteName = reposName
	} else {
		indexName = nameParts[0]
		remoteName = nameParts[1]
	}
	return indexName, remoteName
}

func main() {
	if len(os.Args) < 2 {
		log.Fatalln("Please provider cluster name")
	}
	clusters := os.Args[1:]
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-1")},
	)
	if err != nil {
		log.Fatal(err)
	}
	svc := ecs.New(sess)
	t := table.NewWriter()
	t.SetAllowedRowLength(400)
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "Cluster Name", "Task ID", "Status", "StartDate", "EndDate", "Image"})

	for _, cluster := range clusters {

		tasks := listClusterTasks(svc, cluster)
		// if len(tasks) != 1 {
		// 	log.Panicf("Expected only one task. Got: %d", len(tasks))
		// }
		if err != nil {
			handleAwsErr(err)
			return
		}
		// var runningTask *Task
		for i, task := range tasks {
			data := task.Data(svc)
			// status := task.statusFromData(svc, data)
			// if status == AWS_RUNNING {
			// 	runningTask = task
			// }
			taskarn, _ := arn.Parse(*task.arn)
			startTime := data.PullStartedAt
			startTimeStr := "<nil>"
			if startTime != nil {
				startTimeStr = startTime.Local().Format("Jan 2, 15:04:05")
			}
			endTime := data.StoppedAt
			endTimeStr := "<nil>"
			if endTime != nil {
				endTimeStr = endTime.Local().String()
			}
			imageFull := *data.Containers[0].Image
			_, remoteName := splitReposSearchTerm(imageFull)
			id_list := strings.Split(taskarn.Resource, "/")
			t.AppendRow(table.Row{
				i, cluster, id_list[len(id_list)-1], *data.LastStatus, startTimeStr, endTimeStr, remoteName})
		}

	}
	t.SortBy([]table.SortBy{table.SortBy{Name: "Cluster Name", Mode: table.Dsc}})
	t.Render()
	// if runningTask != nil {
	// fmt.Println("RUNNING: " + *runningTask.arn)
	// data := runningTask.Data(svc)
	// serviceID := *data.StartedBy
	// fmt.Println("StartedBy: " + serviceID)
	// serviceInput := &ecs.ListServicesInput{
	// 	Cluster: data.ClusterArn,
	// 	// Services: []*string{data.StartedBy},
	// }
	// services, _ := svc.ListServices(serviceInput)
	// service := services.ServiceArns[0]
	// servicesDescInput := &ecs.DescribeServicesInput{
	// 	Cluster:  data.ClusterArn,
	// 	Services: []*string{service},
	// }
	// servicesDesc, _ := svc.DescribeServices(servicesDescInput)
	// fmt.Println(servicesDesc)
	// }
	// runningTask.Stop(svc)
}
