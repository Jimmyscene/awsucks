module app

go 1.14

require (
	github.com/aws/aws-sdk-go v1.29.24
	github.com/go-openapi/strfmt v0.19.5 // indirect
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/mattn/go-runewidth v0.0.8 // indirect
)
