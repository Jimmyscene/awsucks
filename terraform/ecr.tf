variable "region" {
  default = "us-east-1"
}

variable "image_path" {
  default = "../test_image/"
}

resource "aws_ecr_repository" "test_image" {
  name                 = "test_image"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}

resource "null_resource" "upload_to_ecr" {
  triggers = {
    main = base64sha256(file("${var.image_path}/main.go"))
  }
  provisioner "local-exec" {
    working_dir = var.image_path
    command     = "./upload.sh ${aws_ecr_repository.test_image.repository_url}"
  }
}

