resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.main.id
}
#
# ALB Security group
resource "aws_security_group" "lb" {
  name        = "fargate-testing-lb"
  description = "controls access to the ALB"
  vpc_id      = aws_vpc.main.id
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "lb_ingress_from_everywhere" {
  security_group_id = aws_security_group.lb.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "lb_egress_to_task" {
  security_group_id        = aws_security_group.lb.id
  type                     = "egress"
  from_port                = var.container_port
  to_port                  = var.container_port
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.ecs_tasks.id

}

# Traffic to the ECS Cluster should only come from the ALB
resource "aws_security_group" "ecs_tasks" {
  name        = "fargate-testing-ecs"
  description = "allow inbound access from the ALB only"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol        = "tcp"
    from_port       = var.container_port
    to_port         = var.container_port
    security_groups = [aws_security_group.lb.id]
  }
  lifecycle {
    create_before_destroy = true
  }
  # Thanks to
  # https://dev.to/danquack/private-fargate-deployment-with-vpc-endpoints-1h0p
  # otherwise i never would've figured this out
  egress {
    protocol        = "tcp"
    from_port       = 443
    to_port         = 443
    security_groups = [aws_security_group.vpc-ecr.id]
  }
  egress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    prefix_list_ids = [aws_vpc_endpoint.s3.prefix_list_id]
  }
}

