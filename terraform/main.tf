provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "~/.aws/creds"
}

locals {
  tags = {
    Name = "fargate-testing"
    env  = "fargate-testing"
  }
}
variable "container_port" { default = 8080 }
variable "az_count" { default = 3 }

resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  tags                 = local.tags
  enable_dns_support   = true
  enable_dns_hostnames = true
}

data aws_availability_zones "available" { state = "available" }

output "vpc_id" {
  value = aws_vpc.main.id
}
