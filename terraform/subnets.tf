resource "aws_subnet" "public" {
  count  = var.az_count
  vpc_id = aws_vpc.main.id

  cidr_block        = cidrsubnet(aws_vpc.main.cidr_block, 8, 20 + count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]

  map_public_ip_on_launch = false
  tags = {
    Name = "fargate-testing-public-subnet-${count.index}"
    env  = "fargate-testing"
  }
}

resource "aws_subnet" "private" {
  count  = var.az_count
  vpc_id = aws_vpc.main.id

  cidr_block        = cidrsubnet(aws_vpc.main.cidr_block, 8, 10 + count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]

  map_public_ip_on_launch = false
  tags = {
    Name = "fargate-testing-private-subnet-${count.index}"
    env  = "fargate-testing"
  }

}


