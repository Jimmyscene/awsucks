#!/bin/sh

base_dns=$1
lb_url=http://$base_dns/api/echo
while  true;
do
    output=$(curl -m 0.5 --fail -s $lb_url)
    if [ $? != 0 ]; then
        notify-send "App monitor" "Failed: Stdout: $output" -u critical -t 3000
        echo $output
        echo "Failed"
    else
    echo "$(date) - success"
    fi
    sleep 5
done
