resource "aws_flow_log" "main" {
  iam_role_arn    = aws_iam_role.fargate_task.arn
  log_destination = aws_cloudwatch_log_group.app.arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.main.id
}
#
