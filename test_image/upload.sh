#!/bin/sh
image=$1
echo $image
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $image
docker build -t $1:latest .
docker push $image:latest
