package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

func echoHandler(w http.ResponseWriter, r *http.Request) {
	data := make(map[string]interface{})
	data["headers"] = r.Header
	dataJSON, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Could not marshal headers"))
		log.Printf("Could not marshal headres: %s", err.Error())
	}
	fmt.Fprintf(w, string(dataJSON))
}
func main() {
	port := os.Getenv("APP_PORT")
	if port == "" {
		log.Fatalf("No App port Specified!")
	}
	log.Printf("Server Started on port: %s", port)
	http.HandleFunc("/api/echo", echoHandler)
	err := http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
	log.Printf("Server exiting")
	log.Fatal(err)

}
