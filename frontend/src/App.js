import React, { PureComponent, Component } from "react";
import {
    ResponsiveContainer,
    PieChart,
    Pie,
    Sector,
    Cell,
    Label
} from "recharts";

const renderActiveShape = props => {
    const RADIAN = Math.PI / 180;
    const {
        cx,
        cy,
        midAngle,
        innerRadius,
        outerRadius,
        startAngle,
        endAngle,
        fill,
        payload,
        percent,
        value
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? "start" : "end";

    return (
        <g>
            <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
                {payload.name}
            </text>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill={fill}
            />
            <Sector
                cx={cx}
                cy={cy}
                startAngle={startAngle}
                endAngle={endAngle}
                innerRadius={outerRadius + 6}
                outerRadius={outerRadius + 10}
                fill={fill}
            />
            <path
                d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
                stroke={fill}
                fill="none"
            />
            <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
            <text
                x={ex + (cos >= 0 ? 1 : -1) * 12}
                y={ey}
                textAnchor={textAnchor}
                fill="#333"
            >{`${value}`}</text>
            <text
                x={ex + (cos >= 0 ? 1 : -1) * 12}
                y={ey}
                dy={18}
                textAnchor={textAnchor}
                fill="#999"
            >
                {`${(percent * 100).toFixed(2)}%`}
            </text>
        </g>
    );
};

export default class App extends Component {
    constructor(props) {
        super(props);
        const data = {
            "Not important cluster": {
                "Service 1": [
                    { name: "Running", value: 4, color: "#5cb85c" },
                    { name: "Pending", value: 2, color: "#f0ad4e" },
                    { name: "Error", value: 20, color: "#d9534f" }
                ],
                "Service 2": [
                    { name: "Running", value: 1, color: "#5cb85c" },
                    { name: "Pending", value: 4, color: "#f0ad4e" },
                    { name: "Error", value: 3, color: "#d9534f" }
                ],
                "Service 3": [
                    { name: "Running", value: 4, color: "#5cb85c" },
                    { name: "Pending", value: 0, color: "#f0ad4e" },
                    { name: "Error", value: 0, color: "#d9534f" }
                ]
            },
            "Very Important Cluster": {
                "Service 1": [
                    { name: "Running", value: 4, color: "#5cb85c" },
                    { name: "Pending", value: 1, color: "#f0ad4e" },
                    { name: "Error", value: 0, color: "#d9534f" }
                ],
                "Service 2": [
                    { name: "Running", value: 2, color: "#5cb85c" },
                    { name: "Pending", value: 2, color: "#f0ad4e" },
                    { name: "Error", value: 4, color: "#d9534f" }
                ],
                "Service 3": [
                    { name: "Running", value: 2, color: "#5cb85c" },
                    { name: "Pending", value: 1, color: "#f0ad4e" },
                    { name: "Error", value: 0, color: "#d9534f" }
                ]
            }
        };
        this.state = { data };
    }
    render() {
        return (
            <div>
                {Object.entries(this.state.data).map(([key, entry]) => (
                    <Cluster data={entry} name={key} />
                ))}
            </div>
        );
    }
}
class Cluster extends Component {
    render() {
        return (
            <div>
                <h1 style={{ display: "block" }}> {this.props.name}</h1>
                <div style={{ display: "flex", flex: "100%" }}>
                    {Object.entries(this.props.data).map(([key, entry]) => (
                        <Example
                            data={entry}
                            name={key}
                            key={"example-" + key}
                        />
                    ))}
                </div>
            </div>
        );
    }
}

class Example extends PureComponent {
    state = {
        activeIndex: null
    };

    onPieEnter = (data, index) => {
        this.setState({
            activeIndex: index
        });
    };

    onPieLeave = () => {
        this.onPieEnter(0, null);
    };
    onClick = ({ name }) => {
        alert(`Taking you to ${name}`);
    };
    render() {
        return (
            <ResponsiveContainer width="25%" height={400}>
                <PieChart width={400} height={400}>
                    <Pie
                        activeIndex={this.state.activeIndex}
                        activeShape={renderActiveShape}
                        data={this.props.data}
                        cx={200}
                        cy={200}
                        innerRadius={60}
                        outerRadius={80}
                        fill="#8884d8"
                        dataKey="value"
                        onMouseEnter={this.onPieEnter}
                        onMouseLeave={this.onPieLeave}
                        onClick={this.onClick}
                    >
                        {this.props.data.map((entry, index) => (
                            <Cell fill={entry.color} key="color-{index}" />
                        ))}
                        {this.state.activeIndex == null && (
                            <Label
                                value={this.props.name + " Tasks"}
                                position="center"
                                onClick={e => {
                                    console.log(e);
                                    alert();
                                }}
                            />
                        )}
                    </Pie>
                </PieChart>
            </ResponsiveContainer>
        );
    }
}
