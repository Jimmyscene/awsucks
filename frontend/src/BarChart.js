import React from "react";
import * as d3 from "d3";

class BarChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
                {
                    title: "Running",
                    value: props.running
                },
                {
                    title: "Error",
                    value: props.error
                },
                {
                    title: "Pending",
                    value: props.pending
                }
            ]
        };
    }
    componentDidMount() {
        this.drawChart2();
    }
    drawChart2() {
        var width = 360;
        var height = 360;
        var radius = Math.min(width, height) / 2;
        var donutWidth = 75;
        var color = d3.scaleOrdinal().range(["#5cb85c", "#d9534f", "#f0ad4e"]);

        var svg = d3
            .select("body")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr(
                "transform",
                "translate(" + width / 2 + "," + height / 2 + ")"
            );

        var arc = d3
            .arc()
            .innerRadius(radius - donutWidth)
            .outerRadius(radius);

        var pie = d3
            .pie()
            .value(function (d) {
                return d.value;
            })
            .sort(null);

        var legendRectSize = 13;
        var legendSpacing = 7;

        var donutTip = d3
            .select("body")
            .append("div")
            .attr("class", "donut-tip")
            .style("opacity", 0);

        var path = svg
            .selectAll("path")
            .data(pie(this.state.data))
            .enter()
            .append("path")
            .attr("d", arc)
            .attr("fill", function (d, i) {
                return color(d.data.title);
            })
            .attr("transform", "translate(0, 0)")
            .on("mouseover", function (d, i) {
                d3.select(this)
                    .transition()
                    .duration("50")
                    .attr("opacity", ".85");
                donutTip.transition().duration(50).style("opacity", 1);
                let num =
                    Math.round((d.value / d.data.all) * 100).toString() + "%";
                donutTip
                    .html(num)
                    .style("left", d3.event.pageX + 10 + "px")
                    .style("top", d3.event.pageY - 15 + "px");
            })
            .on("mouseout", function (d, i) {
                d3.select(this)
                    .transition()
                    .duration("50")
                    .attr("opacity", "1");
                donutTip.transition().duration("50").style("opacity", 0);
            });

        var legend = svg
            .selectAll(".legend")
            .data(color.domain())
            .enter()
            .append("g")
            .attr("class", "circle-legend")
            .attr("transform", function (d, i) {
                var height = legendRectSize + legendSpacing;
                var offset = (height * color.domain().length) / 2;
                var horz = -2 * legendRectSize - 13;
                var vert = i * height - offset;
                return "translate(" + horz + "," + vert + ")";
            });

        legend
            .append("circle")
            .style("fill", color)
            .style("stroke", color)
            .attr("cx", 0)
            .attr("cy", 0)
            .attr("r", ".5rem");

        legend
            .append("text")
            .attr("x", legendRectSize + legendSpacing)
            .attr("y", legendRectSize - legendSpacing)
            .text(function (d) {
                return d;
            });

        const change = data => {
            console.log(data);
            var pie = d3
                .pie()
                .value(function (d) {
                    return d.value;
                })
                .sort(null)(data);

            var width = 360;
            var height = 360;
            var radius = Math.min(width, height) / 2;
            var donutWidth = 75;

            path = d3.select("body").selectAll("path").data(pie); // Compute the new angles
            var arc = d3
                .arc()
                .innerRadius(radius - donutWidth)
                .outerRadius(radius);
            path.transition().duration(500).attr("d", arc); // redrawing the path with a smooth transition
        };

        d3.select("button#reset").on("click", function () {});
        d3.select("button#error").on("click", e => {
            this.setState(
                prevState => {
                    let newState = { ...prevState };
                    newState.data[1].value += 1;
                    return newState;
                },
                () => change(this.state)
            );
        });
        d3.select("button#everyone").on("click", function () {});
    }
    drawChart() {
        // set the dimensions and margins of the graph
        var width = this.props.width;
        var height = this.props.height;
        var margin = 40;

        // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
        var radius = Math.min(width, height) / 2 - margin;

        // append the svg object to the div called 'my_dataviz'
        var svg = d3
            .select("body")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr(
                "transform",
                "translate(" + width / 2 + "," + height / 2 + ")"
            )
            .on("mouseover", function (d) {
                var nodeSelection = d3.select(this).style({ opacity: "0.8" });
                nodeSelection.select("text").style({ opacity: "1.0" });
            });
        // Create dummy data
        var data = {
            running: this.props.running,
            error: this.props.error,
            pending: this.props.pending
        };

        // set the color scale
        var color = d3
            .scaleOrdinal()
            .domain(data)
            .range(["orange", "green", "red"]);

        // Compute the position of each group on the pie:
        var pie = d3.pie().value(function (d) {
            return d.value;
        });
        var data_ready = pie(d3.entries(data));

        // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
        svg.selectAll("whatever")
            .data(data_ready)
            .enter()
            .append("path")
            .attr(
                "d",
                d3
                    .arc()
                    .innerRadius(100) // This is the size of the donut hole
                    .outerRadius(radius)
            )
            .attr("fill", function (d) {
                return color(d.data.key);
            })
            .attr("stroke", "black")
            .style("stroke-width", "2px")
            .style("opacity", 0.7);
    }

    render() {
        return <div id={"#" + this.props.id}> </div>;
    }
}

export default BarChart;
